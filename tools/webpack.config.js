const Webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const UglifyJsWebpackPlugin = require('uglifyjs-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');


const utils = require('./utils');
const config = require('./config');

module.exports = ({production} = {}) => {
    let configuration = {
        mode: production ? 'production' : 'development',
        entry: {
            admin: utils.srcpath('admin'),
            client: utils.srcpath('client')
        },
        output: {
            path: utils.destpath(),
            filename: '[name].js',
            publicPath: config.publicPath
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    loader: 'babel-loader',
                    options: {
                        compact: production,
                        cacheDirectory: true
                    }
                }, {
                    test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
                    loader: 'url-loader',
                    options: {
                        limit: 10240,
                        name: 'images/[hash:8].[ext]'
                    }
                }, {
                    test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
                    loader: 'url-loader',
                    options: {
                        limit: 10240,
                        name: 'fonts/[hash:8].[ext]'
                    }
                },
                ...utils.styleLoaders({
                    sourcemap: config.get('sourcemap', production),
                    production: production
                })
            ]
        },
        resolve: {
            extensions: ['.js', '.less'],
            alias: {
                '@': utils.srcpath(),
                '$variables': utils.srcpath('styles/variables.less'),
                '$mixins': utils.srcpath('styles/mixins.less')
            }
        },
        performance: {
            hints: false
        },
        devtool: config.depended('sourcemap', 'source-map', false, production),
        stats: {
            colors: true,
            modules: false,
            children: false,
            chunks: false,
            chunkModules: false
        },
        cache: !production,
        optimization: {
            namedChunks: true,
            runtimeChunk: {
                name: 'manifest'
            },
            splitChunks: {
                chunks: 'all',
                cacheGroups: {
                    default: false,
                    vendors: {
                        name: 'vendor',
                        test: /[\\/]node_modules[\\/]/,
                        chunks: 'all'
                    }
                }
            },
            minimize: production,
            minimizer: [
                new UglifyJsWebpackPlugin({
                    parallel: true,
                    sourceMap: config.prod.sourcemap,
                    uglifyOptions: {
                        ecma: 8,
                        compress: {
                            warnings: false
                        },
                        output: {
                            comments: false,
                            beautify: false
                        },
                        mangle: {
                            toplevel: true
                        }
                    }
                }),
                new OptimizeCSSAssetsPlugin({
                    cssProcessor: require('cssnano'),
                    cssProcessorOptions: {
                        discardComments: {removeAll: true},
                        zindex: false
                    },
                    canPrint: true
                })
            ]
        },
        plugins: [
            new MiniCssExtractPlugin({
                chunkFilename: '[name].css'
            })
            // ,
            // // new CopyWebpackPlugin([
            // //     {from: utils.srcpath('client/images/page-image/'), to: utils.destpath()},
            // //     {from: utils.srcpath('client/images/'), to: utils.destpath()},
            // //     {from: utils.srcpath('client/favicons'), to: utils.destpath()},
            // //     {from: utils.srcpath('client/docs'), to: utils.destpath()},
            // //     {from: utils.srcpath('client/videos'), to: utils.destpath()}
            // // ])
            ,
            ...(production ? [
                new CleanWebpackPlugin([config.destpath], {
                    root: config.root,
                    verbose: false
                })
            ] : [
                new Webpack.HotModuleReplacementPlugin(),
                new FriendlyErrorsPlugin()
            ])
        ]
    };

    if (!production) {
        Object.keys(configuration.entry).forEach((name) => {
            configuration.entry[name] = ['./tools/dev-client'].concat(configuration.entry[name]);
        });
    }

    return configuration;
};
