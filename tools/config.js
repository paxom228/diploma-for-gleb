const path = require('path');

const config = {
    root: path.resolve(__dirname, '..'),
    srcpath: path.resolve(__dirname, '../assets'),
    destpath: path.resolve(__dirname, '../static'),
    publicPath: '/static/',
    url: 'http://localhost:8090',

    // Development mode
    dev: {
        env: 'development',
        sourcemap: true,
        port: 8090,
        autoOpenBrowser: true,
        proxyTable: [
            [(p) => [/\/static\//, /\/__webpack_hmr/].reduce((a, c) => a && !p.match(c), true),{
                target: 'http://localhost:5000/',
                logLevel: 'warn'
            }]
        ]
    },

    // Production mode
    prod: {
        env: 'production',
        sourcemap: false
    },

    get(name, hasprod=true) {
        return hasprod ? config.prod[name] : config.dev[name];
    },
    depended(conf, dev, prod, hasprod=true) {
        return !hasprod && config.dev[conf] ? dev : (hasprod && config.prod[conf] ? prod : false);
    }
};

module.exports = config;
