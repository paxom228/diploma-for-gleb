import pkgutil
from importlib import import_module


def _discover(package):
    for loader, name, ispkg in pkgutil.iter_modules(package.__path__, f'{package.__name__}.'):
        module = import_module(name)

        if ispkg:
            _discover(module)


def autodiscover():
    from application import views

    _discover(views)
