import re
from collections import OrderedDict

from bson import ObjectId
from pymongo import ReturnDocument

from system.db import mongo


class BaseField:
    type = str
    default = ''

    def __init__(self, blank=True):
        self.blank = blank

    def validate(self, value):
        return None if (self.blank or value) else 'empty'

    def cast(self, value):
        return self.type(value) if value is not None else None

    def get(self, name, dictlike):
        return dictlike.get(name, self.default)


class StringField(BaseField):
    type = str
    default = ''


class IntegerField(BaseField):
    type = int
    default = 0


class ListField(BaseField):
    type = list
    default = []

    def get(self, name, dictlike):
        if hasattr(dictlike, 'getlist'):
            return dictlike.getlist(name)
        return dictlike.get(name, self.default)


class BaseModelMeta(type):
    def __new__(mcs, name, bases, attrs):
        fields = OrderedDict()

        for fname, fval in {**{'_id': StringField()}, **attrs}.items():
            if isinstance(fval, BaseField):
                fields[fname] = fval
                attrs[fname] = fval.default

        attrs['_fields'] = fields

        return super().__new__(mcs, name, bases, attrs)


class BaseModel(metaclass=BaseModelMeta):
    collection = ''
    requred_files = []

    error_messages = {
        'empty': 'Заполните поле',
        'no_file': 'Выберите файл',
    }

    _fields = {}
    _id = StringField()

    def __init__(self, **kwargs):
        self.contribute(kwargs, _instance=self)

    def __str__(self):
        return f'<{self.__class__.__name__}[id={self._id}]>'

    __repr__ = __str__

    @property
    def files(self):
        names = '|'.join(self.requred_files)
        return dict(zip(self.requred_files,
                        self.get_files({'filename': re.compile(rf'{self.collection}-{self._id}-({names})')})))

    def generate_name(self, name):
        return f'{self.collection}-{self._id}-{name}'

    def save_files(self, files):
        for name, file in files.items():
            name = self.generate_name(name)
            self.delete_file(name)
            self.save_file(name, file)
        return self

    @classmethod
    def contribute(cls, valid_data, _instance=None):
        instance = _instance or cls()
        for name, field in cls._fields.items():
            value = valid_data.get(name)
            setattr(instance, name, value)
        return instance

    @classmethod
    def validate(cls, data, files=None):
        _data, _errors, _files = {}, {}, {}

        if isinstance(data, dict):
            for name, field in cls._fields.items():
                value = field.get(name, data)

                if isinstance(field, StringField) and value is not None:
                    value = value.replace('&#39;', '')

                field_error = field.validate(value)
                if not field_error:
                    _data[name] = field.cast(value)
                else:
                    _errors[name] = cls.error_messages.get(field_error)

        if files is not None:
            for name in cls.requred_files:
                if name in files and files[name]:
                    _files[name] = files[name]
                else:
                    _errors[name] = cls.error_messages['no_file']

        return _data, _errors, _files

    @classmethod
    def find(cls, kwargs=None, sort=None, sort_direction=None):
        items = mongo.db[cls.collection].find(kwargs or {})
        if items and sort is not None:
            items = items.sort(*sort) if isinstance(sort, tuple) else items.sort(sort)

        return [cls(**item) for item in (items or [])]

    @classmethod
    def sort(cls, sort_field=None, sort=None):
        items = mongo.db[cls.collection].find({})
        if items and sort is not None:
            items = items.sort(sort_field, int(sort))

        return [cls(**item) for item in (items or [])]

    @classmethod
    def get_one(cls, kwargs=None):
        data = mongo.db[cls.collection].find_one(kwargs or {})
        return cls(**data) if data else None

    @classmethod
    def get(cls, id):
        data = mongo.db[cls.collection].find_one({'_id': ObjectId(id)})
        return cls(**data) if data else None

    def change_fields(self, data):
        if '_id' in data:
            data.pop('_id')
        return self.contribute(
            mongo.db[self.collection].find_one_and_update({'_id': ObjectId(self._id)}, {'$set': data}, return_document=ReturnDocument.AFTER))

    @classmethod
    def delete(cls, id):
        instance = cls.get(id)
        for file in ((instance.files.values() if isinstance(instance.files, dict) else instance.files) if instance else []):
            cls.delete_file(file.get('filename'))
        return mongo.db[cls.collection].delete_one({'_id': ObjectId(id)})

    @classmethod
    def get_files(cls, *args):
        return mongo.db.fs.files.find(*args, {'filename': 1})

    @classmethod
    def delete_file(cls, filename):
        file = mongo.db.fs.files.find_one({'filename': filename})
        if file is not None:
            mongo.db.fs.files.delete_one({'_id': ObjectId(file['_id'])})
        return ''

    @classmethod
    def save_file(cls, filename, content):
        if content is not None:
            return mongo.save_file(str(filename), content)
        return ''

    def save(self):
        if not self._id:
            result = mongo.db[self.collection].insert_one(self.dump(True))
            self._id = str(result.inserted_id)
        else:
            data = self.dump()
            data.pop('_id')
            mongo.db[self.collection].find_one_and_update({'_id': ObjectId(self._id)}, {'$set': data})
        return self

    def dump(self, noid=False):
        result = {}
        for name, field in self._fields.items():

            if not noid or (noid and name != '_id'):
                result[name] = getattr(self, name, field.default)
        return result
