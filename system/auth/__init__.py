from functools import wraps

from flask import redirect, session

from system import config


def auth_required(fn):
    @wraps(fn)
    def wrapped(*args, **kwargs):
        if 'login' in session:
            return fn(*args, **kwargs)
        return redirect(config.LOGIN_URL)

    return wrapped
