from flask import Flask
from system import config
from system.db import init_database
from system.router import Router
from system.views import autodiscover
from system.templating import render

app = Flask(__name__, template_folder='../templates')
app.secret_key = config.SECRET_KEY
app.config['TEMPLATES_AUTO_RELOAD'] = config.DEBUG
app.config['SESSION_TYPE'] = config.SESSION_ENGINE

init_database(app)

router = Router()
autodiscover()

router.register(app)

@app.errorhandler(404)
def page_not_found(e):
    # note that we set the 404 status explicitly

    return render('client/notfound.html', context='client')


init_database(app)

router = Router()
autodiscover()

router.register(app)

@app.errorhandler(404)
def page_not_found(e):
    # note that we set the 404 status explicitly

    return render('client/notfound.html')
