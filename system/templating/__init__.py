from flask import render_template


def _get_context(name):
    try:
        from application import contexts
        if hasattr(contexts, name):
            return getattr(contexts, name)()
        raise KeyError(f'Context "{name}" not found.')
    except ImportError:
        return {}


def render(path, context='', **kwargs):
    return render_template(path, **_get_context(context), **kwargs)
