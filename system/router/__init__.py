from importlib import import_module

from flask import url_for


class Router:
    _prefix_cache = {}
    _routes = []

    @classmethod
    def resolve(cls, name, **kwargs):
        return url_for(name, **kwargs)

    @classmethod
    def get(cls, url, name=None):
        return cls.route(url, ['GET'], name)

    @classmethod
    def post(cls, url, name=None):
        return cls.route(url, ['POST'], name)

    @classmethod
    def route(cls, url, methods, name=None):
        return cls.decorator(url, methods, name)

    @classmethod
    def decorator(cls, url, methods, name):
        def wrapper(fn):
            _url = url

            if (fn.__module__ in cls._prefix_cache):
                _url = f'{cls._prefix_cache[fn.__module__]}{url}'
            else:
                try:
                    module = import_module(fn.__module__)
                    if hasattr(module, 'FILE_URL_PREFIX'):

                        _url = '/'.join([module.FILE_URL_PREFIX.rstrip('/'), url.strip('/')])
                        cls._prefix_cache[fn.__module__] = module.FILE_URL_PREFIX
                except:
                    pass

            cls._routes.append((_url, name, methods, fn))

            def wrapped(*args, **kwargs):
                return fn(*args, **kwargs)

            return wrapped

        return wrapper

    def register(self, app):
        for url, name, methods, fn in self._routes:
            app.route(url, methods=methods, endpoint=name)(fn)
