from flask_pymongo import PyMongo

from system import config

mongo = PyMongo()

def init_database(app):
    mongo.init_app(app, uri=config.MONGO_URL)
