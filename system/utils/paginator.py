class Paginator:
    def __init__(self, items=None, limit=4, page=1):
        self.items = items or []
        self.limit = limit
        self.page = page

    @property
    def page_items(self):
        offset = (self.page - 1) * self.limit
        return self.items[offset:offset + self.limit]

    @property
    def has_pagination(self):
        return len(self.items) > self.limit

    @property
    def num_pages(self):
        return (len(self.items) // self.limit) + (1 if len(self.items) % self.limit else 0)
