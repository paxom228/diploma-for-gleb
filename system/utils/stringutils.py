import random
import re
import string

TRANSLITERATION_RUS = [
    'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'х', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п',
    'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'
]

TRANSLITERATION_ENG = [
    'a', 'b', 'v', 'g', 'd', 'e', 'e', 'zh', 'z', 'i', 'i', 'k', 'l', 'm', 'n', 'o', 'p',
    'r', 's', 't', 'u', 'f', 'h', 'c', 'cz', 'sh', 'sch', '', 'y', '', 'e', 'u', 'ja'
]


def transliterate(string):
    for rus, eng in zip(TRANSLITERATION_RUS, TRANSLITERATION_ENG):
        string = string.replace(rus, eng).replace(rus.upper(), eng.upper())
    return string


def slugify(string):
    return re.sub(r'[^a-z\d\-]+', '', transliterate(re.sub(r'[\s\-+]+', '-', string.lower()))).strip(' -')


def random_string(length):
    return ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(length))
