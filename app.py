from argparse import ArgumentParser

from system import config
from system.server import app

app.debug = config.DEBUG

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('-p', '--port', type=int, action='store', dest='port', default=5000)
    args = parser.parse_args()

    app.run(debug=config.DEBUG, port=args.port)
