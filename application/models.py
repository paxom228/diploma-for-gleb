import re
from collections import Counter

from system.models import BaseModel, ListField, StringField, IntegerField


class User(BaseModel):
    collection = 'user'

    login = StringField(blank=False)
    password = StringField(blank=False)


class People(BaseModel):
    collection = 'people'
    requred_files = ['img']

    full_name = StringField(blank=False)
    description = StringField(blank=False)
    birthday = StringField(blank=False)
    bibliography = StringField(blank=False)
    biography = StringField(blank=False)
    url = StringField(blank=False)

class Photo(BaseModel):
    collection = 'photo'
    requred_files = ['img']
    full_name = StringField(blank=False)

class Video(BaseModel):
    collection = 'video'
    requred_files = ['img']

    full_name = StringField(blank=False)
    url = StringField(blank=False)
