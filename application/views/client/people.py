import re

from flask import request

from application.models import People
from system.router import Router
from system.templating import render
from system.utils.paginator import Paginator

FILE_URL_PREFIX = '/people'


@Router.get('/', name='people:general')
def people():
    sort = request.args.get('sort')
    sort_field = request.args.get('sort-field')
    return render('client/people.html', context='client', paginator=Paginator(People.sort(sort_field, sort) if sort and sort_field else People.find(), page=int(request.args.get('page', 1))))


@Router.get('/<people>', name='people:history')
def people_item(people):
    return render('client/peopleItem.html', context='client', person=People.get_one({'url': people}))


@Router.get('/search', name='catalog:find')
def search():
    query = request.args.get('query', '')

    result = People.find({'$or': [
        {'full_name': re.compile(rf'.*{query}.*', re.IGNORECASE)}
    ]})
    return render('client/search.html', context='client', query=query, result=result)
