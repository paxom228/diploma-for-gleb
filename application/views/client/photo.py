import re

from flask import request

from application.models import Photo
from system.router import Router
from system.templating import render
from system.utils import flat_form
from system.utils.paginator import Paginator

FILE_URL_PREFIX = '/photo'


@Router.route('/', ['GET', 'POST'], name='photo:general')
def people():
    filter = request.args.get('photos') or 'all'
    names = []
    for item in Photo.find():
        names.append(item.full_name)
    if filter == 'all':
        return render('client/photo.html', context='client', data=Photo.find(), names=list(set(names)), selected='all')
    else:
        result = Photo.find({'$or': [
            {'full_name': re.compile(rf'.*{filter}.*', re.IGNORECASE)}
        ]})

        return render('client/photo.html', context='client', data=result, names=list(set(names)), selected=result[0].full_name)
