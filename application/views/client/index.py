from system.router import Router
from system.templating import render

FILE_URL_PREFIX = ''

@Router.get('/', name='people:index')
def index():
    return render('client/index.html')
