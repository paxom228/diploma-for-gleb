import re

from flask import request

from application.models import Video
from system.router import Router
from system.templating import render
from system.utils import flat_form
from system.utils.paginator import Paginator

FILE_URL_PREFIX = '/video'


@Router.route('/', ['GET', 'POST'], name='video:general')
def people():
    filter = request.args.get('video') or 'all'
    names = []
    for item in Video.find():
        names.append(item.full_name)
    if filter == 'all':
        return render('client/video.html', context='client', data=Video.find(), names=list(set(names)), selected='all')
    else:
        result = Video.find({'$or': [
            {'full_name': re.compile(rf'.*{filter}.*', re.IGNORECASE)}
        ]})

        return render('client/video.html', context='client', data=result, names=list(set(names)), selected=result[0].full_name)
