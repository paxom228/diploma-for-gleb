from system.router import Router
from system.templating import render

FILE_URL_PREFIX = '/test'

@Router.get('/', name='test:index')
def index():
    return render('client/test.html')


