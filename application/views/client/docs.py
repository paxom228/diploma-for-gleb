from system.router import Router
from system.templating import render

FILE_URL_PREFIX = '/docs'

@Router.get('/', name='docs:index')
def index():
    return render('client/docs.html')
