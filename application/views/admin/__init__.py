import bcrypt
from flask import redirect, request, session

from application.models import User
from system import config
from system.auth import auth_required
from system.router import Router
from system.templating import render

FILE_URL_PREFIX = config.ADMIN_PREFIX


@Router.get('/', name='admin:index')
@auth_required
def index():
    return render('admin/index.html', context='admin')


@Router.route('/login', ['GET', 'POST'], name='admin:login')
def login():
    if request.method == 'POST':
        login = request.form.get('login')
        password = request.form.get('password')
        user = User.get_one({'login': login})

        if user:
            user_password = user.password if isinstance(user.password, bytes) else user.password.encode()
            if bcrypt.hashpw(password.encode('utf-8'), user_password) == user_password:
                session['login'] = login
                return redirect(Router.resolve('admin:index'))

    return render('admin/login.html', context='admin')


@Router.get('/logout', name='admin:logout')
def logout():
    session.pop('login', None)
    return redirect(Router.resolve('admin:login'))
