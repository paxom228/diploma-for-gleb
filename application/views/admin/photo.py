from datetime import datetime

from flask import redirect, request

from application.models import Photo
from system import config
from system.auth import auth_required
from system.router import Router
from system.templating import render
from system.utils import flat_form
from system.utils.stringutils import slugify

FILE_URL_PREFIX = f'{config.ADMIN_PREFIX}/photo'


# Helper
def save_photo(id=None, edit=False):
    data, errors, files = Photo.validate({
        **flat_form(request),
        'date': datetime.today().strftime('%d.%m.%Y'),
        'url': slugify(request.form['full_name']),
        **({'_id': id} if id else {})
    }, request.files or None)

    if not errors:
        data = Photo(**data).save().save_files(files)

    if len(errors) == 1 and  errors.get('img') and edit:
        data = Photo(**data).save()
        errors = {}

    return data, errors


@Router.get('/', name='admin:photo:index')
@auth_required
def index():
    return render('admin/photo/index.html', context='admin', data=Photo.find())


@Router.route('/add', ['GET', 'POST'], name='admin:photo:add')
@auth_required
def add():
    data, errors = {}, []

    if request.method == 'POST':
        print(request)
        data, errors = save_photo()
        if not errors:
            return redirect(Router.resolve('admin:photo:index'))

    return render('admin/photo/form.html', context='admin', data=data, errors=errors)


@Router.route('/edit:<id>', ['GET', 'POST'], name='admin:photo:edit')
@auth_required
def edit(id=''):
    data, errors = {}, []

    if request.method == 'POST':
        print('post')
        data, errors = save_photo(id, edit=True)

        print(errors)
        if not errors:
            return redirect(Router.resolve('admin:photo:index'))
    else:
        data = Photo.get(id)

    return render('admin/photo/form.html', context='admin', data=data, errors=errors)


@Router.get('/delete:<id>', name='admin:photo:delete')
@auth_required
def delete(id):
    Photo.delete(id)
    return redirect(Router.resolve('admin:photo:index'))
