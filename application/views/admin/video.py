from datetime import datetime

from flask import redirect, request

from application.models import Video
from system import config
from system.auth import auth_required
from system.router import Router
from system.templating import render
from system.utils import flat_form

FILE_URL_PREFIX = f'{config.ADMIN_PREFIX}/video'


# Helper
def save_video(id=None, edit=False):
    data, errors, files = Video.validate({
        **flat_form(request),
        **({'_id': id} if id else {})
    }, request.files or None)

    if not errors:
        data = Video(**data).save().save_files(files)

    return data, errors


@Router.get('/', name='admin:video:index')
@auth_required
def index():
    return render('admin/video/index.html', context='admin', data=Video.find())


@Router.route('/add', ['GET', 'POST'], name='admin:video:add')
@auth_required
def add():
    data, errors = {}, []

    if request.method == 'POST':
        print(request)
        data, errors = save_video()
        if not errors:
            return redirect(Router.resolve('admin:video:index'))

    return render('admin/video/form.html', context='admin', data=data, errors=errors)


@Router.route('/edit:<id>', ['GET', 'POST'], name='admin:video:edit')
@auth_required
def edit(id=''):
    data, errors = {}, []

    if request.method == 'POST':
        print('post')
        data, errors = save_video(id, edit=True)

        print(errors)
        if not errors:
            return redirect(Router.resolve('admin:people:index'))
    else:
        data = Video.get(id)

    return render('admin/video/form.html', context='admin', data=data, errors=errors)


@Router.get('/delete:<id>', name='admin:video:delete')
@auth_required
def delete(id):
    Video.delete(id)
    return redirect(Router.resolve('admin:video:index'))
