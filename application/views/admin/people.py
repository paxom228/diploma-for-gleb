from datetime import datetime

from flask import redirect, request

from application.models import People
from system import config
from system.auth import auth_required
from system.router import Router
from system.templating import render
from system.utils import flat_form
from system.utils.stringutils import slugify

FILE_URL_PREFIX = f'{config.ADMIN_PREFIX}/people'


# Helper
def save_people(id=None, edit=False):
    data, errors, files = People.validate({
        **flat_form(request),
        'date': datetime.today().strftime('%d.%m.%Y'),
        'url': slugify(request.form['full_name']),
        **({'_id': id} if id else {})
    }, request.files or None)

    if not errors:
        data = People(**data).save().save_files(files)

    if len(errors) == 1 and  errors.get('img') and edit:
        data = People(**data).save()
        errors = {}

    return data, errors


@Router.get('/', name='admin:people:index')
@auth_required
def index():
    return render('admin/people/index.html', context='admin', data=People.find())


@Router.route('/add', ['GET', 'POST'], name='admin:people:add')
@auth_required
def add():
    data, errors = {}, []

    if request.method == 'POST':
        print(request)
        data, errors = save_people()
        if not errors:
            return redirect(Router.resolve('admin:people:index'))

    return render('admin/people/form.html', context='admin', data=data, errors=errors)


@Router.route('/edit:<id>', ['GET', 'POST'], name='admin:people:edit')
@auth_required
def edit(id=''):
    data, errors = {}, []

    if request.method == 'POST':
        print('post')
        data, errors = save_people(id, edit=True)

        print(errors)
        if not errors:
            return redirect(Router.resolve('admin:people:index'))
    else:
        data = People.get(id)

    return render('admin/people/form.html', context='admin', data=data, errors=errors)


@Router.get('/delete:<id>', name='admin:people:delete')
@auth_required
def delete(id):
    People.delete(id)
    return redirect(Router.resolve('admin:people:index'))
