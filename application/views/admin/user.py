import bcrypt
from flask import redirect, request

from application.models import User
from system import config
from system.router import Router
from system.templating import render

FILE_URL_PREFIX = f'{config.ADMIN_PREFIX}/users'


@Router.get('/', name='admin:users:index')
def index():
    return render('admin/users/index.html', context='admin', data=User.find())

@Router.route('/add', ['GET', 'POST'], name='admin:users:add')
def add():
    data, errors = {}, []

    if request.method == 'POST':
        login = request.form['login']
        password = request.form['password']
        existing_user = User.find({'login': login})

        if not existing_user:
            hashpass = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())
            User(login=login, password=hashpass).save()

            return redirect(Router.resolve('admin:users:index'))
        return 'Пользователь уже существует'
    return render('admin/users/form.html', context='admin', data=data, errors=errors)


@Router.get('/delete:<id>', name='admin:users:delete')
def delete(id):
    User.delete(id)
    return redirect(Router.resolve('admin:users:index'))
