from flask import request

from system.db import mongo
from system.router import Router


@Router.get('/media/<id>')
def media(id):
    return mongo.send_file(id)


@Router.post('/media/add/img')
def addimg():
    print(request.form)
