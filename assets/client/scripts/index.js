window.onload = function() {
    document.querySelectorAll('.person-btn').forEach((item) => {
        item.addEventListener('click', (e) => {
            document.querySelectorAll('.person-btn').forEach((el) => {
                if (el.className.includes('active')) {
                    el.classList.remove('active');
                }
            });
            item.classList.add('active');
            document.querySelectorAll('.text').forEach((el) => {
                if (el.className.includes('active')) {
                    el.classList.remove('active');
                }
            });
            document.querySelector(`#${item.dataset.person}`).classList.add('active');
        });
    });
    document.querySelectorAll('.string-search').forEach(search => {
        search.addEventListener('keydown', (e) => {
            if (e.keyCode === 13) {
                e.preventDefault();
                window.location = '/people/search?query=' + search.value;
            }
        });
    });

    // dropdown

    document.querySelectorAll('.dropdown-activator').forEach((activator) => {
        activator.addEventListener('click', () => {
            document.querySelector(`#${activator.dataset.dropdown}`).classList.toggle('active');
        });
    });

    //filter photo
    let photo_form = document.getElementById('filter-photos');

    if (photo_form) {
        photo_form.querySelector('select').addEventListener('change', (e) => {
            photo_form.submit();
        });
    }

    let video_form = document.getElementById('filter-video');

    if (video_form) {
        video_form.querySelector('select').addEventListener('change', (e) => {
            video_form.submit();
        });
    }

    // test
    const answers = ['3', '4', '2', '1'];
    let listStatus = document.querySelector('.questions-list');

    document.querySelectorAll('.answer-btn').forEach((answer) => {
        answer.addEventListener('click', () => {
            let status = false;
            let listAnswer = answer.parentElement;
            let questionNum = +listAnswer.dataset.question;
            let answerNum = answer.dataset.answer;

            if (answers[questionNum] === answerNum) {
                status = true;
                answer.classList.add('success');
            } else {
                listAnswer.querySelector(`[data-answer="${answers[questionNum]}"]`).classList.add('success');
                answer.classList.add('error');
            }

            listStatus.querySelector(`[data-question="${questionNum}"]`).classList.add(`${status ? 'success' : 'error'}`);

            listAnswer.classList.add('disabled');
            answer.removeEventListener('click');
        });
    });
};
