import $ from 'jquery';

export default () => {
    $('form').on('change', '.img', function() {
        $(this).parents('.image-uploading-field').find('.image-name').html($(this).val().replace(/.*\\/, ''));
    }).on('click', '.del', function() {
        $(this).parent().remove();
    }).on('click', '.delete-img', function(e) {
        e.preventDefault();
        $('form').append(`<input type="hidden" name="delete" value="${$(this).data('name')}">`);
        $(this).parents('.image-uploading-field').remove();
    });

    $('.add-slide').on('click', function(e) {
        e.preventDefault();
        $('.slides').append(`
            <div class="image-uploading-field">
                <label><input type="file" class="img" name="img${$(this).data('index')}">Выбрать</label>
                <div class="image-name">Файл не выбран</div>
            </div>
        `);
        $(this).data('index', $(this).data('index') + 1);
    });
};
