import {randomstr} from '../utils/utils';
import '../libraries/plugin';

export default () => {
    try {
        document.querySelectorAll('.submit-page-form').forEach((btn) => {
            btn.addEventListener('click', (e) => {
                e.preventDefault();
                document.querySelector(btn.dataset.form ? `#${btn.dataset.form}` : 'form').submit();
            });
        });

        document.querySelectorAll('.page-back').forEach((btn) => {
            btn.addEventListener('click', (e) => {
                e.preventDefault();
                history.back();
            });
        });

        document.querySelectorAll('[data-ckedit]').forEach((item) => {
            if (!item.id) {
                item.id = randomstr();
            }

            window.CKEDITOR.replace(item.id, {extraPlugins: 'imageUploader'});
        });

        let href = window.location.href;
        let needActive = href.match(/catalog\//) ? 'catalog' : href.match(/users/) ? 'users' : 'general';
        document.querySelector('.sidebar-menu .active').classList.remove('active');
        document.querySelector(`.sidebar-menu #${needActive}`).classList.add('active');
    } catch (e) { /* ignore */ }
};
