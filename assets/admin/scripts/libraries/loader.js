import $ from 'jquery';
import Noty from 'noty';

export default class Loader {
    static start() {
        $('.loading-wrapper').css({'display': 'block'});
    }
    static stop(text, type = 'success', url = '') {
        if (url === '') {
            new Noty({type: type, text: text}).show();
        } else {
            new Noty({
                type,
                text,
                timeout: 1500,
                callbacks: {
                    afterShow() {
                        window.location.href = url;
                    }
                }
            }).show();
        }
        $('.loading-wrapper').css({'display': 'none'});
    }
}
