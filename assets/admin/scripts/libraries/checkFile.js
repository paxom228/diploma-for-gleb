import $ from 'jquery';
import Noty from 'noty';

export const checkFile = (FILE, type = 'application/pdf') => {
    let file = $(FILE).get(0).files[0];
    if (type === 'application/pdf' && file.type === type) {
        if (file.size > 5000000) {
            new Noty({type: 'error', text: 'Ошибка: файл слишком большой'}).show();
            $(FILE).val('');
        } else return true;
    } else if (file.type.match('image*') && type !== 'application/pdf') {
        if (file.size > 5000000) {
            new Noty({type: 'error', text: 'Ошибка: файл слишком большой'}).show();
            $(FILE).val('');
        } else return true;
    } else {
        $(FILE).val('');
        new Noty({type: 'error', text: `Ошибка: тип файла не ${type}`}).show();
    }
    return false;
};


