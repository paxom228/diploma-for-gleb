import $ from 'jquery';
import Noty from 'noty';
import config from '@/admin/scripts/config';
import Loader from '@/admin/scripts/libraries/loader';

export default class Http {
    static request(url, data = {}, type = 'GET', additional = {}) {

        return $.ajax({
            url,
            type,
            data: data instanceof FormData ? data : JSON.stringify(data),
            dataType: 'json',
            contentType: 'application/json',
            ...additional
        });
    }

    static apiRequest(url, data = {}, type = 'GET', addditional = {}, noty = true) {
        Loader.start();
        return new Promise((resolve, reject) => {
            Http.request(`${config.adminPrefix}/api${url.trim('/')}`, data, type, addditional).then((data) => {
                if (noty) {
                    if (data.error) {
                        new Noty({type: 'error', text: data.error}).show();
                        reject(data);
                    } else {
                        resolve(data);
                    }
                } else {
                    resolve(data);
                }
            }, (err) => {
                if (noty) {
                    Loader.stop('Произошла ошибка на сервере. Свяжитесь со службой техподдержки.' , 'error');
                }
                reject(err);
            });
        });
    }
}
