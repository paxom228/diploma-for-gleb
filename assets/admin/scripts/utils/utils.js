export const randomstr = (len = 8) => {
    const chars = 'abcdefghijklmnopqrstuvwxys';
    let result = '';
    for (let i = 0; i < len; i++) {
        result += chars[Math.floor(Math.random() * chars.length)];
    }
    return result;
};
