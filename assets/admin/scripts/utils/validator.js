export const isNoEmptyObj = (obj) => {
    if (obj != 'undefined') {
        return Object.values(obj).every(val => val !== '');
    }
    return false;
};
export const validEmail = (email) => /^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/.test(email);
